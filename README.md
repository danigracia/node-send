# NodeSend

Aplicación web desarrollada con el MERN Stack para enviar archivos entre usuarios. Sistema de autenticación, roles y permisos

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas_


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```sh
sudo apt install nodejs
sudo apt install npm
```

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_Clonar el respositorio_

```sh
git clone https://gitlab.com/danigracia/node-send.git
cd node-send
```

_Iniciar el servidor del Backend_

```
cd backend
npm install
npm run dev
```

_Iniciar el servidor del Frontend_

```
cd frontend
npm install
npm run dev
```

_Ya tendríamos una copia del repositorio funcionando en nuestra máquina_


## Construido con 🛠️

* [NextJS](https://nextjs.org/) - El framework usado para el frontend
* [NodeJS](https://nodejs.org/es/) - Lenguaje de backend
* [Express](https://expressjs.com/es/) - Servidor de backend
* [MongoDB](https://www.mongodb.com/) - Base de datos

## Autores ✒️

* **Dani Gracia** - [danigracia](https://gitlab.com/danigracia)

## Licencia 📄

Este proyecto está bajo la Licencia M.I.T. - mira el archivo [LICENSE.md](LICENSE.md) para detalles

---
⌨️ con ❤️ por [danigracia](https://github.com/danigracia)
