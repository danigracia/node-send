import "../styles/globals.css";
import AppState from "../context/app/appState";
import AuthState from "../context/auth/authState";

function MyApp({ Component, pageProps }) {
    return (
        <AppState>
            <AuthState>
                <Component {...pageProps} />
            </AuthState>
        </AppState>
    );
}

export default MyApp;
