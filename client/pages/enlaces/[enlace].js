import React, { useState } from "react";
import Layout from "../../components/layout/Layout";
import clienteAxios from "../../config/axios";
import Alerta from "../../components/ui/Alerta";

export async function getServerSidePaths() {
    const respuesta = await clienteAxios.get("/enlaces");
    console.log(respuesta.data);
    return {
        paths: respuesta.data.enlaces.map((enlace) => ({
            params: { enlace: enlace.url },
        })),
        fallback: false,
    };
}

export async function getServerSideProps({ params }) {
    const resultado = await clienteAxios.get(`/enlaces/${params.enlace}`);
    return {
        props: {
            enlace: resultado.data,
        },
    };
}

export default function Enlace({ enlace }) {
    const [archivo, setArchivo] = useState(enlace.archivo);
    const [tienePassword, setTienePassword] = useState(enlace.password);
    const [error, setError] = useState(null);
    const [password, setPassword] = useState("");

    const verificarPassword = async (e) => {
        e.preventDefault();

        try {
            const respuesta = await clienteAxios.post(`enlaces/${archivo}`, {
                password,
            });
            setTienePassword(respuesta.data.password);
            setArchivo(respuesta.data.archivo);
        } catch (error) {
            setError({
                tipo: "error",
                mensaje: error.response.data.error,
            });
        }
    };

    return (
        <Layout>
            {tienePassword ? (
                <>
                    <h2 className="text-2xl font-sans font-bold text-gray-800 text-center my-4">
                        Este enlace esta protegido por un password
                    </h2>

                    {error && <Alerta alerta={error} />}

                    <div className="flex justify-center mt-5">
                        <div className="w-full max-w-lg">
                            <form
                                className="bg-white rounded shadow-md px-8 pt-6 pb-8 mb-4"
                                onSubmit={verificarPassword}
                            >
                                <div className="mb-4">
                                    <label
                                        htmlFor="password"
                                        className="block text-gray-800 text-sm font-bold mb-2"
                                    >
                                        Password
                                    </label>
                                    <input
                                        type="password"
                                        name="password"
                                        id="password"
                                        className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                        placeholder="Password del archivo"
                                        value={password}
                                        onChange={(e) =>
                                            setPassword(e.target.value)
                                        }
                                    />
                                </div>
                                <input
                                    type="submit"
                                    className="py-3 px-5 block w-full rounded-lg bg-blue-500 hover:bg-blue-700 hover:cursor-pointer text-white font-bold uppercase"
                                    value="Validar Password"
                                />
                            </form>
                        </div>
                    </div>
                </>
            ) : (
                <>
                    <h1 className="text-4xl text-center text-gray-700">
                        Descarga tu archivo
                    </h1>
                    <div className="flex items-center justify-center mt-10">
                        <a
                            href={`${process.env.backendURL}/archivos/${archivo}`}
                            className="bg-blue-500 text-center px-10 py-3 rounded-lg uppercase text-white font-bold hover:cursor-pointer"
                        >
                            Descargar
                        </a>
                    </div>
                </>
            )}
        </Layout>
    );
}
