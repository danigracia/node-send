import React, { useContext, useEffect } from "react";
import Layout from "../components/layout/Layout";
import Link from "next/link";

//Context
import authContext from "../context/auth/authContext";
import appContext from "../context/app/appContext";

//Componentes
import Dropzone from "../components/Dropzone";
import Alerta from "../components/ui/Alerta";
import CopiarEnlace from "../components/CopiarEnlace";

export default function Home() {
    //Extraer el usuario autenticado del storage
    const AuthContext = useContext(authContext);
    const { usuarioAutenticado } = AuthContext;

    const AppContext = useContext(appContext);
    const { alerta_archivo, url } = AppContext;

    useEffect(() => {
        const token = localStorage.getItem("token");

        if (token) {
            usuarioAutenticado();
        }
    }, []);

    return (
        <Layout>
            <div className="md:w-4/5 xl:w-3/5 mx-auto mb-32">
                {url ? (
                    <CopiarEnlace url={url} />
                ) : (
                    <>
                        {alerta_archivo && <Alerta alerta={alerta_archivo} />}
                        <div className="lg:flex gap-4 md:shadow-lg bg-white rounded-lg px-5 py-10">
                            <Dropzone />

                            <div className="md:flex-1 mb-3 mx-2 mt-16 lg:mt-0">
                                <h2 className="text-4xl font-sans font-bold text-gray-800 my-4">
                                    Compartir archivos de forma sencilla y
                                    privada
                                </h2>
                                <p className="text-lg leading-loose">
                                    <span className="text-blue-500 font-bold">
                                        ReactNodeSend
                                    </span>{" "}
                                    te permite compartir archivos con cifrado de
                                    extremo a extremo y un archivo que es
                                    eliminado después de ser descargado. Así
                                    puedes mantener lo que compartes privado y
                                    asegurarte que tus cosas no permanezcan en
                                    línea para siempre.
                                </p>
                                <Link href="/crear-cuenta">
                                    <a className="text-blue-500 hover:text-blue-700 text-lg">
                                        Crea una cuenta para mayores beneficios
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </>
                )}
            </div>
        </Layout>
    );
}
