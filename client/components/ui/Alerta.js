import React from "react";

export default function Alerta({ alerta }) {
    const { mensaje, tipo } = alerta;

    return (
        <div
            className={`py-2 px-3 w-full my-3 max-w-lg text-center text-white mx-auto ${
                tipo === "exito" ? "bg-blue-500" : "bg-red-500"
            }`}
        >
            {mensaje}
        </div>
    );
}
