import React from "react";

export default function Enlace({ url }) {
    const enlace = process.env.frontendURL + "/enlaces/" + url;

    return (
        <div className="flex flex-col items-center justify-center shadow-lg bg-white rounded-lg px-5 py-10">
            <p className="text-center text-2xl">
                <span className="font-bold text-blue-700 text-3xl uppercase">
                    Tu url es:
                </span>
                {enlace}
            </p>
            <button
                type="button"
                className="mt-10 py-3 px-5 block md:w-72 w-full rounded-lg bg-blue-500 hover:bg-blue-700 hover:cursor-pointer text-white font-bold uppercase"
                onClick={() => navigator.clipboard.writeText(enlace)}
            >
                Copiar Enlace
            </button>
        </div>
    );
}
