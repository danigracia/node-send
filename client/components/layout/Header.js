import React, { useContext } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

//Context
import authContext from "../../context/auth/authContext";
import appContext from "../../context/app/appContext";

export default function Header() {
    const router = useRouter();

    //Acceder al state
    const AuthContext = useContext(authContext);
    const { autenticado, cerrarSesion } = AuthContext;

    //Context app
    const AppContext = useContext(appContext);
    const { limpiarState } = AppContext;

    const redirrecionar = () => {
        router.push("/");
        limpiarState();
    };

    return (
        <header className="py-8 flex flex-col md:flex-row items-center justify-between">
            <img
                className="w-64 mb-8 md:mb-0 hover:cursor-pointer"
                src="/logo.svg"
                alt="Logo Página"
                onClick={() => redirrecionar()}
            />

            <div className="flex flex-col md:flex-row gap-4">
                {autenticado ? (
                    <>
                        <button
                            className="py-3 px-5 rounded-lg bg-blue-500 hover:bg-blue-700 text-white font-bold uppercase"
                            onClick={(e) => cerrarSesion()}
                        >
                            Cerrar Sesión
                        </button>
                    </>
                ) : (
                    <>
                        <Link href="/login">
                            <a className="py-3 px-5 rounded-lg bg-blue-500 hover:bg-blue-700 text-white font-bold uppercase">
                                Iniciar Sesión
                            </a>
                        </Link>
                        <Link href="/crear-cuenta">
                            <a className="py-3 px-5 rounded-lg bg-gray-800 hover:bg-gray-600 text-white font-bold uppercase">
                                Crear Cuenta
                            </a>
                        </Link>
                    </>
                )}
            </div>
        </header>
    );
}
