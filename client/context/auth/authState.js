import react, { useReducer } from "react";
import authContext from "./authContext";
import authReducer from "./authReducer";
import {
    CERRAR_SESION,
    LIMPIAR_ALERTA,
    LOGIN_ERROR,
    LOGIN_EXITO,
    REGISTRO_ERROR,
    REGISTRO_EXITO,
    USUARIO_AUTENTICADO,
} from "./authTypes";

import clienteAxios from "../../config/axios";
import tokenAuth from "../../config/tokenAuth";

const AuthState = ({ children }) => {
    //State inicial
    const initialState = {
        token: typeof window !== "undefined" && localStorage.getItem("token"),
        autenticado: null,
        usuario: null,
        alerta: null,
    };

    //Definir reducer
    const [state, dispatch] = useReducer(authReducer, initialState);

    //Registrar usuario
    const registrarUsuario = async (datos) => {
        try {
            const respuesta = await clienteAxios.post("/usuarios/", datos);
            dispatch({
                type: REGISTRO_EXITO,
                payload: { mensaje: respuesta.data.msg, tipo: "exito" },
            });
        } catch (error) {
            dispatch({
                type: REGISTRO_ERROR,
                payload: { mensaje: error.response.data.error, tipo: "error" },
            });
        }

        setTimeout(() => {
            dispatch({
                type: LIMPIAR_ALERTA,
            });
        }, 2000);
    };

    //Autenticar usuario
    const iniciarSesion = async (datos) => {
        try {
            const respuesta = await clienteAxios.post("/auth", datos);
            dispatch({
                type: LOGIN_EXITO,
                payload: respuesta.data.token,
            });
        } catch (error) {
            dispatch({
                type: LOGIN_ERROR,
                payload: { mensaje: error.response.data.error, tipo: "error" },
            });
        }

        setTimeout(() => {
            dispatch({
                type: LIMPIAR_ALERTA,
            });
        }, 2000);
    };

    //Usuario autenticado en base al JWT del LS
    const usuarioAutenticado = async () => {
        const token = localStorage.getItem("token");
        if (token) {
            tokenAuth(token);
        }

        try {
            const respuesta = await clienteAxios.get("/auth");
            dispatch({
                type: USUARIO_AUTENTICADO,
                payload: respuesta.data,
            });
        } catch (error) {
            return;
        }
    };

    //Cerrar sesión
    const cerrarSesion = async () => {
        dispatch({
            type: CERRAR_SESION,
        });
    };

    return (
        <authContext.Provider
            value={{
                token: state.token,
                autenticado: state.autenticado,
                usuario: state.usuario,
                alerta: state.alerta,
                registrarUsuario,
                iniciarSesion,
                usuarioAutenticado,
                cerrarSesion,
            }}
        >
            {children}
        </authContext.Provider>
    );
};

export default AuthState;
