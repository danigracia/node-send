//Registro usuarios
export const REGISTRO_EXITO = "REGISTRO_EXITO";
export const REGISTRO_ERROR = "REGISTRO_ERROR";

//Autenticación
export const LOGIN_EXITO = "LOGIN_EXITO";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const USUARIO_AUTENTICADO = "USUARIO_AUTENTICADO";

//Cerrar sesion
export const CERRAR_SESION = "CERRAR_SESION";

//Otros
export const LIMPIAR_ALERTA = "LIMPIAR_ALERTA";
