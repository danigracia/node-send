import {
    REGISTRO_ERROR,
    REGISTRO_EXITO,
    LIMPIAR_ALERTA,
    LOGIN_ERROR,
    LOGIN_EXITO,
    USUARIO_AUTENTICADO,
    CERRAR_SESION,
} from "./authTypes";

const authReducer = (state, action) => {
    switch (action.type) {
        case REGISTRO_EXITO:
        case REGISTRO_ERROR:
        case LOGIN_ERROR:
            return {
                ...state,
                alerta: action.payload,
            };

        case LOGIN_EXITO:
            localStorage.setItem("token", action.payload);
            return {
                ...state,
                alerta: null,
                autenticado: true,
                token: action.payload,
            };

        case LIMPIAR_ALERTA:
            return {
                ...state,
                alerta: null,
            };

        case USUARIO_AUTENTICADO:
            return {
                ...state,
                autenticado: true,
                token: localStorage.getItem("token"),
                usuario: action.payload,
            };

        case CERRAR_SESION:
            localStorage.removeItem("token");
            return {
                ...state,
                autenticado: null,
                token: null,
                usuario: null,
            };

        default:
            return state;
    }
};

export default authReducer;
