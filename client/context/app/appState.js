import React, { useReducer } from "react";
import appContext from "./appContext";
import appReducer from "./appReducer";
import {
    SUBIR_ARCHIVO,
    SUBIR_ARCHIVO_EXITO,
    SUBIR_ARCHIVO_ERROR,
    CREAR_ENLACE_EXITO,
    CREAR_ENLACE_ERROR,
    MOSTRAR_ALERTA,
    LIMPIAR_ALERTA,
    LIMPIAR_STATE,
    AGREGAR_PASSWORD,
    AGREGAR_DESCARGAS,
} from "./appTypes";
import clienteAxios from "../../config/axios";

const AppState = ({ children }) => {
    const initialState = {
        alerta_archivo: null,
        nombre: "",
        nombre_original: "",
        cargando: false,
        descargas: 1,
        password: "",
        autor: null,
        url: "",
    };

    //Crear state y dispatch
    const [state, dispatch] = useReducer(appReducer, initialState);

    const mostrarAlerta = (alerta) => {
        dispatch({
            type: MOSTRAR_ALERTA,
            payload: alerta,
        });

        setTimeout(() => {
            dispatch({
                type: LIMPIAR_ALERTA,
            });
        }, 3000);
    };

    //Subir los archivos al servidor
    const subirArchivo = async (formData, nombreArchivo) => {
        dispatch({
            type: SUBIR_ARCHIVO,
        });

        try {
            const respuesta = await clienteAxios.post("/archivos", formData);
            dispatch({
                type: SUBIR_ARCHIVO_EXITO,
                payload: {
                    nombre: respuesta.data.archivo,
                    nombre_original: nombreArchivo,
                },
            });
        } catch (error) {
            dispatch({
                type: SUBIR_ARCHIVO_ERROR,
                payload: {
                    mensaje: "Hubo un error al intentar subir el archivo",
                    tipo: "error",
                },
            });
        }
    };

    //Crear enlace
    const crearEnlace = async () => {
        const data = {
            nombre: state.nombre,
            nombre_original: state.nombre_original,
            descargas: state.descargas,
            password: state.password,
            autor: state.autor,
        };

        try {
            const respuesta = await clienteAxios.post("/enlaces/", data);
            dispatch({
                type: CREAR_ENLACE_EXITO,
                payload: respuesta.data.url,
            });
        } catch (error) {
            dispatch({
                type: CREAR_ENLACE_ERROR,
                payload: {
                    mensaje: "Hubo un error al intentar obtener el enlace",
                    tipo: "error",
                },
            });
        }
    };

    //Resetea el state
    const limpiarState = () => {
        dispatch({
            type: LIMPIAR_STATE,
        });
    };

    //Agrega el password al state
    const agregarPassword = (password) => {
        dispatch({
            type: AGREGAR_PASSWORD,
            payload: password,
        });
    };

    //Agrega las descargas al state
    const agregarDescargas = (descargas) => {
        dispatch({
            type: AGREGAR_DESCARGAS,
            payload: descargas,
        });
    };

    return (
        <appContext.Provider
            value={{
                alerta_archivo: state.alerta_archivo,
                nombre: state.nombre,
                nombre_original: state.nombre_original,
                cargando: state.cargando,
                descargas: state.descargas,
                password: state.password,
                autor: state.autor,
                url: state.url,
                mostrarAlerta,
                subirArchivo,
                crearEnlace,
                limpiarState,
                agregarPassword,
                agregarDescargas,
            }}
        >
            {children}
        </appContext.Provider>
    );
};

export default AppState;
