const router = require("express").Router();
const { check } = require("express-validator");
const auth = require("../middlewares/auth");
const enlaceContoller = require("../controllers/enlaceContoller");

router.post(
    "/",
    [
        check("nombre", "Sube un archivo").not().isEmpty(),
        check("nombre_original", "Sube un archivo").not().isEmpty(),
    ],
    auth,
    enlaceContoller.crearEnlace
);

router.get("/", enlaceContoller.obtenerEnlaces);

router.get(
    "/:url",
    enlaceContoller.tienePassword,
    enlaceContoller.obtenerEnlace
);

//Validar password
router.post(
    "/:url",
    enlaceContoller.validarPassword,
    enlaceContoller.obtenerEnlace
);

module.exports = router;
