const router = require("express").Router();
const { check } = require("express-validator");
const auth = require("../middlewares/auth");
const archivoController = require("../controllers/archivoController");

//Subida de archivos
router.post("/", auth, archivoController.subirArchivo);

router.get(
    "/:archivo",
    archivoController.descargar,
    archivoController.eliminarArchivo
);

module.exports = router;
