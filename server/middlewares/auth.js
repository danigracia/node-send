const jwt = require("jsonwebtoken");
require("dotenv").config({ path: "variables.env" });

const auth = (req, res, next) => {
    const authHeader = req.get("Authorization");
    if (authHeader) {
        const token = authHeader.split(" ")[1];

        if (token) {
            //Verificar token
            try {
                const usuario = jwt.verify(token, process.env.SECRET_KEY);
                req.usuario = usuario;
            } catch (error) {
                console.log(error);
                console.log("El token no es válido");
            }
        }
    }

    next();
};

module.exports = auth;
