const Enlace = require("../models/Enlace");
const { validationResult } = require("express-validator");
const bcrypt = require("bcrypt");
const shortid = require("shortid");

exports.crearEnlace = async (req, res) => {
    //Mostrar errores de express validator
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(401).json({ errores: errores.array() });
    }

    const { nombre, nombre_original } = req.body;

    //Crear un nuevo objeto de enlace
    const enlace = new Enlace();
    enlace.url = shortid.generate();
    enlace.nombre = nombre;
    enlace.nombre_original = nombre_original;

    //Comprobar si hay un usuario autenticado
    if (req.usuario) {
        const { password, descargas } = req.body;

        //Asignar el numero de descargas
        if (descargas) {
            enlace.descargas = descargas;
        }

        if (password) {
            //Hashear password
            enlace.password = bcrypt.hashSync(password, 10);
        }

        //Asignar el autor
        enlace.autor = req.usuario.id;
    }

    //Almacenar en la DB
    try {
        await enlace.save();
        res.json({ url: enlace.url });
    } catch (error) {
        console.log(error);
        res.status(500).send({ error: "Hubo un error en el servidor" });
    }
};

//Obtiene un listado de todos los enlaces
exports.obtenerEnlaces = async (req, res) => {
    try {
        const enlaces = await Enlace.find({}).select("nombre -_id");
        res.json({ enlaces });
    } catch (error) {
        console.log(error);
        res.status(500).send({ error: "Hubo un error en el servidor" });
    }
};

//Retorna si el enlace tiene password o no
exports.tienePassword = async (req, res, next) => {
    //Verificar si existe el enlace
    const enlace = await Enlace.findOne({ url: req.params.url });

    if (!enlace) {
        return res.status(404).json({ error: "El enlace no existe" });
    }

    if (enlace.password) {
        return res.json({ archivo: enlace.url, password: true });
    }

    next();
};

//Verifica si un password es correcto
exports.validarPassword = async (req, res, next) => {
    const enlace = await Enlace.findOne({ url: req.params.url });

    //Validar password
    if (bcrypt.compareSync(req.body.password, enlace.password)) {
        //Descargar el archivo
        next();
    } else {
        return res.status(401).send({ error: "El password es incorrecto" });
    }
};

//
exports.obtenerEnlace = async (req, res) => {
    //Verificar si existe el enlace
    const enlace = await Enlace.findOne({ url: req.params.url });

    if (!enlace) {
        return res.status(404).json({ error: "El enlace no existe" });
    }

    res.json({ archivo: enlace.nombre, password: false });
};
