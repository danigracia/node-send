const multer = require("multer");
const shortid = require("shortid");
const fs = require("fs");
const Enlace = require("../models/Enlace");

exports.subirArchivo = async (req, res) => {
    configMulter = {
        limits: { fileSize: req.usuario ? 1024 * 1024 * 10 : 1024 * 1024 * 2 },
        storage: (fileStorage = multer.diskStorage({
            destination: (req, file, cb) => {
                cb(null, __dirname + "/../uploads");
            },
            filename: (req, file, cb) => {
                const extension = file.originalname.substring(
                    file.originalname.lastIndexOf("."),
                    file.originalname.length
                );
                cb(null, shortid.generate() + extension);
            },
        })),
    };

    const upload = multer(configMulter).single("archivo");

    upload(req, res, async (error) => {
        if (!error) {
            res.json({ archivo: req.file.filename });
        } else {
            console.log(error);
            res.status(400).send({ error: "El archivo es demasiado grande" });
        }
    });
};

exports.eliminarArchivo = async (req, res) => {
    console.log(req.archivo);
    try {
        fs.unlinkSync(__dirname + `/../uploads/${req.archivo}`);
        console.log("eliminado");
    } catch (error) {
        console.log(error);
    }
};

//Descarga un archivo
exports.descargar = async (req, res, next) => {
    //Obtiene el enlace
    const { archivo } = req.params;
    const enlace = await Enlace.findOne({ nombre: req.params.archivo });

    if (!enlace) {
        return res.status(404).send("El archivo no existe");
    }

    const archivoDescarga = __dirname + "/../uploads/" + archivo;
    res.download(archivoDescarga);

    //Eliminar el archivo y la entrada de la base de datos

    //Si las descargas == 1, eliminar enlace
    if (enlace.descargas === 1) {
        req.archivo = enlace.nombre;

        //Eliminar la entrada de la DB
        await Enlace.findOneAndRemove({ nombre: archivo });
        next();
    }
    //Si descargas > 1, restar una descarga
    else {
        enlace.descargas--;
        await enlace.save();
    }
};
